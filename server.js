var express = require("express");
var bodyParser = require('body-parser');
var app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

var reset = () => {
	var today = new Date();
	var d = {
		event: {
			id: 'event1',
			name: 'Mon événement',
			poster_url: 'https://qontrol.onrender.com/images/poster.png'
		},
		structures: [
			{
				id: 'structure1',
				name: 'Structure 1'
			},
			{
				id: 'structure2',
				name: 'Structure 2'
			}
		],
		categories: [
			{
				id: 'category1',
				name: 'Soldat'
			},
			{
				id: 'category2',
				name: 'Boxeur'
			},
			{
				id: 'category3',
				name: 'Alpiniste'
			},
			{
				id: 'category4',
				name: 'Pilote'
			}
		],
		participants: [
			{
				id: 'participant1', 
				badge_id: 'badge1',
				structure_id: 'structure1',
				category_id: 'category1',
				first_name: 'John',
				last_name: 'Rambo',
				photo_url: 'https://qontrol.onrender.com/images/rambo.png',                    
			},
			{
				id: 'participant2', 
				badge_id: 'badge2',
				structure_id: 'structure1',
				category_id: 'category2',
				first_name: 'Rocky',
				last_name: 'Balboa',
				photo_url: 'https://qontrol.onrender.com/images/rocky.png',
			},
			{
				id: 'participant3', 
				badge_id: 'badge3',
				structure_id: 'structure1',
				category_id: 'category3',
				first_name: 'Gabe',
				last_name: 'Walker',
				photo_url: 'https://qontrol.onrender.com/images/cliffhanger.png',
			},
			{
				id: '5c189cf0-87ff-4dbe-9a26-cf215aa81c7e', 
				badge_id: 'badge4',
				structure_id: 'structure2',
				category_id: 'category1',
				first_name: 'Marion',
				last_name: 'Cobretti',
				photo_url: undefined
			},
			{
				id: 'participant5', 
				badge_id: 'badge5',
				structure_id: 'structure2',
				category_id: 'category4',
				first_name: 'Joe',
				last_name: 'Tanto',
				photo_url: 'https://qontrol.onrender.com/images/driven.png',
			},
			{
				id: 'participant6', 
				badge_id: 'badge6',
				structure_id: 'structure1',
				category_id: 'category2',
				first_name: 'Lincoln',
				last_name: 'Hawk',
				photo_url: 'https://qontrol.onrender.com/images/over_the_top.png',
			},
			{
				id: 'participant7', 
				badge_id: 'badge7',
				structure_id: 'structure2',
				category_id: 'category1',
				first_name: 'John',
				last_name: 'Spartan',
				photo_url: 'https://qontrol.onrender.com/images/demolition_man.png',
			},
			{
				id: 'participant8', 
				badge_id: 'badge8',
				structure_id: 'structure2',
				category_id: 'category1',
				first_name: 'Robert',
				last_name: 'Hatch',
				photo_url: 'https://qontrol.onrender.com/images/escape_to_victory.png',
			},
			{
				id: 'participant9', 
				badge_id: 'badge9',
				structure_id: 'structure2',
				category_id: 'category1',
				first_name: 'Joe',
				last_name: 'Smith',
				photo_url: 'https://qontrol.onrender.com/images/samaritan.png',
			},
			{
				id: 'participant10', 
				badge_id: 'badge10',
				structure_id: 'structure2',
				category_id: 'category1',
				first_name: 'Kit',
				last_name: 'Latura',
				photo_url: 'https://qontrol.onrender.com/images/daylight.png',
			},
			{
				id: 'participant11', 
				badge_id: 'badge11',
				structure_id: 'structure2',
				category_id: 'category1',
				first_name: 'Raymond',
				last_name: 'Tango',
				photo_url: 'https://qontrol.onrender.com/images/tango.png',
			}
		],
		areas: [
			{
				id: 'area1', 
				name: 'A1'
			},
			{
				id: 'area2', 
				name: 'B2'
			}
		],
		meals: [
			{
				id: 'meal1', 
				catering_place_name: { key: 'Le Gunay', map: { fr: 'Le Gunay', en: 'The Gunay' } },
				type: 'BREAKFAST',
				name: 'Repas 1'
			},
			{
				id: 'meal2', 
				catering_place_name: { key: 'Le Good Time', map: { fr: 'Le Good Time', en: 'The Good Time' } },
				type: 'LUNCH',
				name: 'Repas 2'
			},
			{
				id: 'meal3', 
				catering_place_name: { key: 'Le Good Time', map: { fr: 'Le Good Time', en: 'The Good Time' } },
				type: 'DINNER',
				name: ''
			},
			{
				id: 'meal4', 
				catering_place_name: { key: 'Chez Johnny', map: { fr: 'Chez Johnny', en: 'Johnny\'s place' } },
				type: 'DINNER',
				name: 'Goûter'
			}
		],
		parkings: [
			{
				id: 'parking1', 
				name: 'P1'
			}, 
			{
				id: 'parking2', 
				name: 'P2'                
			}
		],
		area_accreditations: [
			{
				participant_id: 'participant1',
				area_id: 'area1',
				start: new Date('01/01/2024'),
				end: new Date('12/31/2024'),
				checked_in: false
			}
		],
		meal_accreditations: [
			{
				participant_id: 'participant1',
				meal_id: 'meal1',
				start: new Date((today.getMonth() + 1) + '/' + today.getDate() + '/' + today.getFullYear() + ' 07:00'),
				end: new Date((today.getMonth() + 1) + '/' + today.getDate() + '/' + today.getFullYear() + ' 09:30'),
				checked_in: false
			},
			{
				participant_id: 'participant1',
				meal_id: 'meal2',
				start: new Date((today.getMonth() + 1) + '/' + today.getDate() + '/' + today.getFullYear() + ' 11:00'),
				end: new Date((today.getMonth() + 1) + '/' + today.getDate() + '/' + today.getFullYear() + ' 13:00'),
				checked_in: false
			},
			{
				participant_id: 'participant1',
				meal_id: 'meal3',
				start: new Date((today.getMonth() + 1) + '/' + today.getDate() + '/' + today.getFullYear() + ' 15:00'),
				end: new Date((today.getMonth() + 1) + '/' + today.getDate() + '/' + today.getFullYear() + ' 19:00'),
				checked_in: false
			},
			{
				participant_id: 'participant1',
				meal_id: 'meal4',
				start: new Date((today.getMonth() + 1) + '/' + today.getDate() + '/' + today.getFullYear() + ' 15:00'),
				end: new Date((today.getMonth() + 1) + '/' + today.getDate() + '/' + today.getFullYear() + ' 15:30'),
				checked_in: false
			}
		],
		parking_accreditations: [],
		operators: [
			{
				id: 'operator1',
				participant_id: 'participant1',
				authorized_areas: ['area1', 'area2'],
				authorized_meals: ['meal1', 'meal2', 'meal3', 'meal4'],
				authorized_parkings: ['parking1', 'parking2']
			},
			{
				id: 'operator2',
				participant_id: 'participant2',
				authorized_areas: [],
				authorized_meals: [],
				authorized_parkings: ['parking1', 'parking2']
			}
		]
	};
	for(var i=12; i<1001; i++) {
		d.participants.push({
			id: 'participant' + i, 
			badge_id: 'badge'+ i,
			structure_id: 'structure2',
			category_id: 'category1',
			first_name: 'Sylvester ' + i,
			last_name: 'Stallone',
			//photoUrl: 'https://qontrol.onrender.com/images/sly.jpg',
			photo_url: 'https://qontrol.onrender.com/images/sylvester_stallone.jpg',
		});
	}
	for(let participant of d.participants) {
		d.area_accreditations.push({
			participant_id: participant.id,
			area_id: 'area2',
			start: new Date('01/01/2024'),
			end: new Date('12/31/2024'),
			checked_in: false
		});
	}
	return d;
};

var data = reset();
app.listen(8080, () => {
 console.log("Server running on port 8080");
});
app.use(function (req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
	res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
	res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With');
    next();
});
app.get("/data", (req, res, next) => {
    console.log("asking for data", new Date());
    res.json(data);
});
app.post("/scans", (req, res, next) => {
    console.log("asking for update participant scans");
	for(let scan of req.body) {
		if(scan.scan_result == 'CHECKIN') {
			var accreditation = undefined;
			var timestamp = new Date(scan.timestamp);
			switch(scan.accreditation_type) {
				case 'AREA': 
					accreditation = data.area_accreditations.find(x => 
						x.participant_id == scan.participant_id && 
						x.area_id == scan.accreditation_object_id &&
						x.start <= timestamp && timestamp <= x.end);
					break;
				case 'MEAL': 
					accreditation = data.meal_accreditations.find(x => 
						x.participant_id == scan.participant_id && 
						x.meal_id == scan.accreditation_object_id &&
						x.start <= timestamp && timestamp <= x.end);
					break;
				case 'PARKING': 
					accreditation = data.parking_accreditations.find(x => 
						x.participant_id == scan.participant_id && 
						x.parking_id == scan.accreditation_object_id &&
						x.start <= timestamp && timestamp <= x.end);
					break;
			}
			if(accreditation) {
				accreditation.checked_in = true;
			}
		}
	}
    res.json(true);
});
app.get("/reset", (req, res, next) => {
	console.log("asking for reset");
    data = reset();
    res.json(true);
});
app.use(express.static('public')); 
app.use('/images', express.static('images'));